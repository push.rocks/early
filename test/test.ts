import { expect, expectAsync, tap } from '@pushrocks/tapbundle';
import * as smartdelay from '@pushrocks/smartdelay';

import * as early from '../ts/index.js';

tap.test('.start()', async () => {
  process.env.CLI_CALL_MODULENAME = 'early';
  early.start('early');
  await smartdelay.delayFor(2000);
});

tap.test('.stop()', async () => {
  await early.stop();
});

tap.test('hrTime Measurement', async () => {
  let earlyHr = new early.HrtMeasurement();
  earlyHr.start();
  await smartdelay.delayFor(1000);
  let measuredTime = earlyHr.stop();
  console.log(measuredTime);
  return expect(measuredTime.milliSeconds).toBeGreaterThan(999);
});

tap.start();
