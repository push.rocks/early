/**
 * autocreated commitinfo by @pushrocks/commitinfo
 */
export const commitinfo = {
  name: '@pushrocks/early',
  version: '4.0.3',
  description: 'minimal and fast loading plugin for startup time measuring'
}
